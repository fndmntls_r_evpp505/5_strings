# Strings

This repo contains the lessons on working with strings in R.

Refer to the chapters on strings in R for Data Science:

- [Strings](http://r4ds.had.co.nz/strings.html)

You can also look at the [cheatsheet](https://github.com/rstudio/cheatsheets/raw/master/strings.pdf) for the `stringr` package we will be using in class.  There is a copy of the cheatsheet in the repo, but always check the site for the latest version; these packages change often.